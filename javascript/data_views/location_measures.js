// Generated by CoffeeScript 1.10.0
(function() {
  module.exports = {
    _id: '_design/location',
    views: {
      'weather': {
        map: (function(doc) {
          if (doc.type === "measure" && doc.name === 'weather') {
            return emit([doc.location_id, doc.timestamp], doc.value);
          }
        }).toString()
      },
      'outside-temperature': {
        map: (function(doc) {
          if (doc.type === "measure" && doc.name === 'outside-temperature') {
            return emit([doc.location_id, doc.timestamp], doc);
          }
        }).toString()
      },
      'temperature': {
        map: (function(doc) {
          if (doc.type === "measure" && doc.name === 'temperature') {
            return emit([doc.location_id, doc.timestamp], doc);
          }
        }).toString()
      }
    }
  };

}).call(this);
