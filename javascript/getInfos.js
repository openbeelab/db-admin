// Generated by CoffeeScript 1.10.0
(function() {
  var config, dbServer, err, error;

  config = require('./config');

  try {
    dbServer = require('../../util/javascript/dbDriver').connectToServer(config.database);
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
    dbServer.databases().then(function(dbs) {
      return console.log(dbs);
    });
  } catch (error) {
    err = error;
    console.log(err);
  }

}).call(this);
