// Generated by CoffeeScript 1.10.0
(function() {
  var config, create_bundle, dbServer, err, error, objects;

  require('../../util/javascript/arrayUtils').install();

  require('../../util/javascript/stringUtils').install();

  require('../../util/javascript/numberUtils').install();

  config = require('./config');

  create_bundle = require('./create_bundle');

  try {
    dbServer = require('../../util/javascript/dbDriver').connectToServer(config.database);
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
    objects = require('../../util/javascript/fixtures')();
    create_bundle(config, dbServer, objects).then(function(logs) {
      var i, len, log, results;
      results = [];
      for (i = 0, len = logs.length; i < len; i++) {
        log = logs[i];
        results.push(console.log(log));
      }
      return results;
    })["catch"](function(err) {
      return console.log(err);
    });
  } catch (error) {
    err = error;
    console.log(err);
  }

}).call(this);
