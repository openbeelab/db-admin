Feature: creating the initial databases
    As a db admin
    I want to create the databases needed for the project and set it up

    Background:
        Given an instance of couchdb
        Given i have admin rights

    Scenario Outline: creating the config and data databases
        When i write in the fixtures file the infos of the fixtures:
            |fixture       |
            |location      |
            |stand         |
            |beehouse      |
            |beehousemodel |
         And i write in the config file the project name <project_name>
         And i launch the bundle script
        Then the project config db exists
         And the project data db exists
         And the users db contains users with roles:
            |user_role|
            |admin    |
            |uploader |

         And the config db has views:
            |view             |
            |stand/all        |
            |stand/location   |
            |location/all     |
            |location/stand   |
            |beehouse/all     |
            |beehouse/stand   |
            |beehouse/location|

        And the config db contains the fixtures:
            |fixture        |
            |location       |
            |noised_location|
            |stand          |
            |beehouse       |

         And the data db has views:
            |view                |
            |measure/all         |
            |measure/by_date     |
            |measure/deltas      |
            |location/temperature|
            |beehouse/weight     |

        Examples:
            |project_name|
            |cucumberTest|
