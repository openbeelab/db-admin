
require('../../util/javascript/arrayUtils').install()
require('../../util/javascript/stringUtils').install()
require('../../util/javascript/numberUtils').install()
#require('../../util/javascript/objectUtils').install()

config = require './config'
create_bundle = require './create_bundle'
try
    dbServer = require('../../util/javascript/dbDriver').connectToServer(config.database)
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0'
    objects = require('../../util/javascript/fixtures')()
    #console.log(objects.location)
    create_bundle(config,dbServer,objects)
    #Promise.resolve(1)
    .then (logs)->
        for log in logs
            console.log log
    .catch (err)->
        console.log err
catch err
    console.log err
