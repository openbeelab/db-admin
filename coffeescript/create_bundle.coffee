util = require 'util'

module.exports = (config,dbServer,fixtures)->
    
    logs = []

    createViews = require './create_views'
    createUsers = require './create_users'
    buildDbSecurityObject = require './buildDbSecurityObject'
    Promise = require "promise"

    dbName = config.database.name
    
    usersDb = dbServer.useDb("_users")
    configDb = dbServer.useDb(config.database.name + "_config")
    dataDb = dbServer.useDb(config.database.name + "_data")
    
    logs = []
    configDb.create()
    .then ->
 
        logs.push "config db created."
        configDb = dbServer.useDb(config.database.name + "_config")
        dataDb.create()
    
    .then ()->

        logs.push "data db created."
        dataDb = dbServer.useDb(config.database.name + "_data")
        secu = config.database.securityObject
        secu = buildDbSecurityObject(secu,config.database.name)
        #Promise.all([configDb.save(secu),dataDb.save(secu)])
        Promise.all([configDb.save(secu)])
    .then ()->
        logs.push "security object created, dbs are protected."
        createViews(configDb,"config")

    .then ()->

        logs.push "config db views created."
        createUsers(usersDb,dbName)

    .then (users)->
        
        logs.push "admin credentials:"
        logs.push ("login:" + users.admin.name + ",password:"+users.admin.password)
        logs.push "uploader credentials:"
        logs.push ("login:" + users.uploader.name + ",password:"+users.uploader.password)
        logs.push "users created."
        
        usersDb.get("org.couchdb.user:remy")
        .then (remy)->
            if not remy.logins?
                remy.logins = {}
            remy.logins[config.database.host + "/" + users.admin.name] = users.admin.password
            remy.logins[config.database.host + "/" + users.uploader.name] = users.uploader.password
            usersDb.save remy
        .catch (err)->
            console.log err

        location = Object.create(fixtures.location)
        create_noised_area = location.create_noised_area
        delete location.create_noised_area
        
        locationPromise = Promise.resolve(location)
        if create_noised_area?
    
            noisedLocation = Object.create(location)
            #noisedLocation._id += "_noised"
            noisedLocation.longitude += Number.getRandomBetween(-1.0*noisedLocation.noise/2.0,noisedLocation.noise/2.0)
            noisedLocation.latitude  += Number.getRandomBetween(-1.0*noisedLocation.noise/2.0,noisedLocation.noise/2.0)
            noisedLocation.locationType = "noisedGPS"
            delete noisedLocation.name
            delete noisedLocation.noise
            delete location.noise
        
        
            locationPromise.then ()->
            
                configDb.save(noisedLocation)
            
            .then (noisedLocation) ->

                logs.push "noised location created."
                location.noisedLocation = noisedLocation._id
                return
        
        locationPromise.then () ->

            configDb.save(location)

    .then ()->
        
        logs.push "location created."
        p1 = configDb.save(fixtures.beehousemodel)
        p2 = configDb.save(fixtures.beehouse)

        return Promise.all([p1,p2])

    .then ()->

        logs.push "beehouse created."
        configDb.save fixtures.stand
        
    .then () ->

        logs.push "stand created."
        createViews(dataDb,"data")

    .then () ->

        logs.push "data db views created"
        return logs

#    .catch (err)->
#    
#        console.log("err:" + util.inspect(err,true,5,true))
