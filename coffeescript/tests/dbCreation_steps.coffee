require('../../../util/javascript/arrayUtils').install()
require('../../../util/javascript/stringUtils').install()
require('../../../util/javascript/numberUtils').install()
#require('../../../util/javascript/objectUtils').install()
Promise = require 'promise'
world = require '../../../util/javascript/tests/features/support/world'
#world.fixtures = require('../../../util/javascript/fixtures')()
expect = require 'must'

module.exports = () ->

    projectName = null
    config =
        database :

            name : null
            auth:
                username: 'admin'
                password: 'osef'
        
            securityObject :
                _id : "_security"
                admins :
                    names : ["admin"]
                    roles : ["admin"]
            
                members :
                    names : []
                    roles : []
    
    @Given /^i have admin rights$/,->

        world.usersDb.get 'org.couchdb.user:'+config.database.auth.username
        .then (my)->
            my.roles.must.include("admin")
    
    @Given /^i write in the fixtures file the infos of the fixtures:$/,(fixtures)->

        for [fixture] in fixtures.rows()
           
            expect(world.fixtures[fixture]).to.exist()
            world.fixtures[fixture].type.must.be(fixture)

    @When /^i write in the config file the project name (.*)$/,(project_name)->

        projectName = config.database.name = project_name

    @When /^i launch the bundle script$/,->
        
        createBundle = require '../create_bundle'
        createBundle(config,world.dbServer,world.fixtures)

    @Then /^the project (.*) db exists$/,(type)->

        db = world[type+'Db'] = world.dbServer.useDb(projectName + "_" + type)
        expect(db?.get).to.exist()

    @Then /^the users db contains users with roles:$/,(roles)->

        rolesPromises = []
        for [role] in roles.rows()
            
            do (role)->

                p = world.usersDb.get('org.couchdb.user:' + projectName + "_" + role)
                .then (user)->
                    expect(user).to.exist()
                    user.roles.must.contain(projectName + "/" + role)
                rolesPromises.push p

        return Promise.all(rolesPromises)

    @Then /^the db named (.*) exists$/,(dbName)->
        
        world.dbServer.useDb(dbName).exists()
        .then (dbExist)->
            dbExist.must.be.true()

    @Then /^the (.*) db has views:$/,(type,views)->

        for [view] in views.rows()
            
            do (view)->

                [prefix,suffix] = view.split("/")
                world[type + 'Db'].get ('_design/' + prefix + '/_view/' + suffix)
                .then (v)->
                    expect(v?._rev).to.exist()
                    
    @Then /^the config db contains the fixtures:$/,(fixtures)->

        for [fixture] in fixtures.rows()

            do (fixture)->
                #console.log fixture
                #return Promise.resolve("pending")
                ;
