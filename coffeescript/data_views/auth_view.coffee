module.exports =

    _id : '_design/auth'

    views : {}
    validate_doc_update : ((newDoc, oldDoc, userCtx,secObj)->
            
            isServerAdmin = userCtx?.roles?.indexOf("_admin") != -1
            if isServerAdmin
                return

            isLogged = userCtx?.name != ''
            if not isLogged
                throw({'forbidden': 'please log in.'})

            dbName = userCtx.db.split("_")[0]
            isDeletion = newDoc.deleted or newDoc._deleted
            isCreation = oldDoc is null
            isUpdate = not (isCreation or isDeletion)

            isUploader = userCtx.roles.some (dbAndRole)->
                [db,role] = dbAndRole.split("/")
                return (db is dbName) and (role is "uploader")

            if isUploader and not (isCreation and newDoc.type is "measure")
                throw({'forbidden': "you can only create measure."})

            hasMemberRole = userCtx.roles.some (dbAndRole)->
                [db,_] = dbAndRole.split("/")
                return (db is dbName)

            isMember = (secObj?.members?.names?.indexOf(userCtx?.name) != -1)
            if not (isMember or hasMemberRole)
                throw({'forbidden': 'you are not allowed to modify this database.'})

        ).toString()
