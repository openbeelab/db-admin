module.exports =
    _id : '_design/location'
    views :

        'weather' :

            map : ((doc)->
                if doc.type == "measure" and doc.name is 'weather'
                    emit [doc.location_id,doc.timestamp], doc.value
                ).toString()

        'outside-temperature' :

            map : ((doc)->

                if doc.type == "measure" and doc.name is 'outside-temperature'
                    emit [doc.location_id,doc.timestamp], doc
                ).toString()

        'temperature' :

            map : ((doc)->

                if doc.type == "measure" and doc.name is 'temperature'
                    emit [doc.location_id,doc.timestamp], doc
                ).toString()
